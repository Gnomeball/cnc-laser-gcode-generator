## CNC Laser GCode Generator

I finally got around to updating my old version, by writing it this time in C++.  It's a bit faster but still needs a lot of work.

I'll get around to finishing it over the next few days and weeks once I can run the files on my home-made CNC Laser.

### Running it

You'll need a C++ compiler, the ImageMagick libraries, and more patience than you can wave a stick at.

### Output

Can be seen in raw form in `/gcode` but also below .. dun dun dunnn!

![Duck!](Duck_GCode.png "Duck!")
